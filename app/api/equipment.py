from flask import request, jsonify, url_for
from app.models import Equipment
from app.api import bp
from app import db
from app.api.errors import bad_request


@bp.route('/equipment/<int:id>', methods=['GET'])
def get_equipment(id):
    # Возвращаем пользователю информацию о машине
    # Если машины с таким уникальным идентификатором нет,
    # возвращаем HTTP 404
    return jsonify(Equipment.query.get_or_404(id).to_dict())

@bp.route('/equipments', methods=['GET'])
def get_equipments():
    # Какую страницу показать? По умолчанию 1
    page = request.args.get('page', 1, type=int)
    # Сколько элементов на странице? По умолчанию 10
    # Но не больше 100
    per_page = min(request.args.get('per_page', 10), 100)
    # Генерируем набор данных для страницы
    data = Equipment.to_collection_dict(Equipment.query, page, per_page, 'api.get_equipments')
    # Возвращаем пользователю json
    return jsonify(data)

@bp.route('/equipments', methods=['POST'])
def create_equipment():
    # Проверяем, что запрос пришел с телом
    data = request.get_json() or {}
    # Если тела запроса нет, возвращаем ошибку
    if not data:
        return bad_request('Equipment should contain something')
    # Создаем экземпляр модели ORM для машины
    equipment = Equipment()
    # Загружаем данные из тела запроса в экземпляр
    equipment.from_dict(data)
    # Добавляем машину к текущей сессии с БД 
    db.session.add(equipment)
    # Комиттим транзакцию в БД
    db.session.commit()
    # По стандарту мы должны вернуть объект
    # с присвоенным уникальным идентификатором
    response = jsonify(equipment.to_dict())
    # Так же по стандарту код ответа должен быть 201 вместо 200
    # 200 OK
    # 201 Created
    response.status_code = 201
    # В заголовке передаем ссылку на созданный объект
    response.headers['Location'] = url_for('api.get_equipment', id=equipment.id)
    return response

@bp.route('/equipments/<int:id>', methods=['PUT'])
def update_equipment(id):
    equipment = Equipment.query.get_or_404(id)
    data = request.get_json() or {}
    if not data:
        return bad_request('Build should contain something')
    equipment.from_dict(data)
    db.session.commit()
    return jsonify(equipment.to_dict())

@bp.route('/equipments/<int:id>', methods=['DELETE'])
def remove_equipment(id):
    equipment = Equipment.query.get_or_404(id)
    db.session.delete(equipment)
    db.session.commit()
    return jsonify(equipment.to_dict())